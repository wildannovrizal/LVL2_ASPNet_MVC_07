use db_target

select * from tbl_student
select NISN, Nama, Kelamin, Tingkat from tbl_student

USE [NawaDataFramework7]
GO
/****** Object:  Table [dbo].[Foods]    Script Date: 29/07/2021 08:43:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Foods](
	[pk_id_foods] [int] IDENTITY(1,1) NOT NULL,
	[item] [char](20) NULL,
	[qty] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[pk_id_foods] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Foods] ON 

INSERT [dbo].[Foods] ([pk_id_foods], [item], [qty]) VALUES (1, N'Sandwiches          ', 23)
INSERT [dbo].[Foods] ([pk_id_foods], [item], [qty]) VALUES (2, N'Salads              ', 20)
INSERT [dbo].[Foods] ([pk_id_foods], [item], [qty]) VALUES (3, N'Soups               ', 15)
INSERT [dbo].[Foods] ([pk_id_foods], [item], [qty]) VALUES (4, N'Beverages           ', 18)
INSERT [dbo].[Foods] ([pk_id_foods], [item], [qty]) VALUES (5, N'Desserts            ', 24)
SET IDENTITY_INSERT [dbo].[Foods] OFF

select * from Foods
